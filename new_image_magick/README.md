ImageMagick Ansible Role (ubuntu flavour)
=========

Install ImageMagick from source

Requirements
------------

None.

Role Variables
--------------

```
imagemagick_version: "7.0.7"

imagemagick_configure_options: ""
imagemagick_install_path: "/opt/imagemagick"
imagemagick_source_path: "/usr/local/src"

imagemagick_optional_dependencies: []
```

Dependencies
------------

None.

Example Playbook
----------------

```
- hosts: servers
  roles:
    - { role: imagemagick }
```

License
-------
Hemant khokhar (hemant.khokhar@opstree.com)
